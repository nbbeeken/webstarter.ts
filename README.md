# Typescript Web Starter

Run:

```sh
npm i
npm start
```

and open a browser to http://localhost:8080

Code in `src/`, `public/index.html`, and `public/main.css`.
